import {Link} from "react-router-dom";
import React from "react";

const Profile = () => {
    return (
        <div>
            <h3>User Profile</h3>
            <ul>
                <li>
                    <Link to='/user-profiles/1'>User Profile 1</Link>
                </li>
                <li>
                    <Link to='/user-profiles/2'>User Profile 2</Link>
                </li>
            </ul>
        </div>
    );
};

export default Profile;
