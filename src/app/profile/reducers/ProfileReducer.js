import {LIKE, PROFILE} from "../actions/ProfileActions";

const initState = {
    profileDetails: {}
};

export default function profileReducer(state = initState, aciton) {
    switch (aciton.type) {
        case PROFILE:
            return {
                ...state,
                profileDetails: aciton.payload
            };
        case LIKE:
            return {
                ...state,
                profileDetails: {
                    ...state.profileDetails,
                    value: aciton.payload.value,
                    disabled: aciton.payload.disabled
                }
            };
        default:
            return state;
    }

}
