const BASE_URL = 'http://127.0.0.1:8080/api';
export const PROFILE = 'PROFILE';
export const LIKE = 'LIKE';

function getProfileDetails(id) {
    return (dispatch) => {
        const URL = BASE_URL + `/user-profiles/${id}`;
        fetch(URL)
            .then(response => response.json())
            .then(json => {
                const {name, gender, description} = json;
                dispatch({
                    type: PROFILE,
                    payload: {
                        name,
                        gender,
                        description,
                        value: 'Like',
                        disabled: false
                    }
                })
            });
    }
}

function markLike(id, name) {
    return (dispatch) => {
        const URL = BASE_URL + '/like';
        fetch(URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({userProfileId: id, userName: name})
        }).then(() => {
            dispatch({
                type: LIKE,
                payload: {
                    value: 'Liked',
                    disabled: true
                }
            })
        })
    };
}

export {
    getProfileDetails,
    markLike
}
