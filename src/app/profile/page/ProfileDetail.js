import {Link} from "react-router-dom";
import React from "react";
import {connect} from "react-redux";
import {getProfileDetails, markLike} from "../actions/ProfileActions";

class ProfileDetail extends React.Component {

    render() {
        const {markLike} = this.props;
        const {name, gender, description, value, disabled} = this.props.profileDetails;
        const {id} = this.props.match.params;
        return (
            <div className="profile">
                <h1>User Profile</h1>
                <ul>
                    <li>User Name: {name}</li>
                    <li>Gender: {gender}</li>
                    <li>Description: <p>{description}</p></li>
                </ul>
                <button onClick={() => markLike(id, 'anonymous user')} disabled={disabled}>{value}</button>
                <Link to="/">&lt; Back to Home</Link>
            </div>
        );
    }

    componentDidMount() {
        const {id} = this.props.match.params;
        const {getProfileDetails} = this.props;
        getProfileDetails(id);
    }
}

const mapStateToProps = ({profile}) => ({
    profileDetails: profile.profileDetails,
});

const mapDispatchToProps = (dispatch) => ({
    getProfileDetails: (id) => dispatch(getProfileDetails(id)),
    markLike: (id, name) => dispatch(markLike(id, name))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileDetail);
