import React from 'react';
import ProductDetails from "./product/pages/ProductDetails";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "./home/Home";
import Profile from "./profile/components/Profile";
import ProfileDetail from "./profile/page/ProfileDetail";
import './App.css'

const App = () => {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route path="/product-details" component={ProductDetails}/>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/user-profiles/:id" component={ProfileDetail}/>
                </Switch>
                <Route exact path="/" component={Profile}/>
            </Router>
        </div>
    );
};

export default App;
